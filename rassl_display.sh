#!/bin/bash

if [ $1 = 'on' ]; then
  sudo tvservice -p;
  # restart slideshow
  sudo pkill fbi
  echo 'HDMI ON!'
fi

if [ $1 = 'off' ]; then
  sudo tvservice -o
  echo 'HDMI OFF!'
fi
