#!/bin/bash

RASSL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

SLIDES_DIR="$RASSL_DIR/slides"

DELAY=10
BLEND=1000

if [ "$1" != "" ]; then
  set DELAY=$1
fi
if [ "$2" != "" ]; then
  set BLEND=$2
fi

LONG_DELAY=$((2*$DELAY))

echo "DELAY=$DELAY"
echo "LONG_DELAY=$LONG_DELAY"
echo "BLEND=$BLEND"

while [  1 -lt 10 ]; do
  # show images
  fbi -1 -noverbose -a -blend $BLEND -t $LONG_DELAY -l $SLIDES_DIR/fl_last_10.txt

  fbi -1 -noverbose -a -blend $BLEND -t $LONG_DELAY -l $SLIDES_DIR/fl_last_20.txt

  fbi -1 -noverbose -a -blend $BLEND -t $LONG_DELAY -l $SLIDES_DIR/fl_last_40.txt

  fbi -1 -random -noverbose -a -blend $BLEND -t $DELAY -l $SLIDES_DIR/fl_all.txt
done
