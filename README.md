# RASSL - Raspberry Pi Simple Slideshow #

## Requiremnts ##

- Raspberry Pi 3
- HDMI Display

## Installation ##

### Prerequisites ###

Use raspi-config to configure
- Boot to CLI
- Automatic login for the **pi** user

### Installing required packages ###

    rassl_setup.sh

### Setup email collector ###

A fetchmail / procmail combination is used to automatically extract attachments of emails.
In the folder there is an example for a POP enabled GMail account.
To use this example add the contents of templates/fetchmailrc to ~/.fetchmailrc and templates/procmailrc to ~/.procmailrc. (Create the files if they do not already exist)
Replace GMAIL_EMAIL and GMAIL_PASSWD in .fetchmailrc with the correct credentials.

### Starting slideshow ###

Add to the /home/pi/.profile file...

    # start fetchmail (only if email collector is used)
    fetchmail
    # start slideshow in endless loop
    # "pkill fbi" or pressing q or ESC will restart the slideshow and read in new images
    <path>/rassl_start.sh <view_time_in_seconds> <blend_time_in_milli_seconds>

Example

    # show each slide 5s and blend over with 500ms
    rassl/rassl_start.sh 5 500

### Extras ###

## Turn off HDMI display regularly ##

To save energy, the HDMI display can be turned off via crontab.
Login as **pi** user and edit crontab using **crontab -e**

    # Turn on display between 6am and 7am
    0 6 * * * /home/pi/rassl/rassl_display.sh on >/dev/null 2>&1
    0 7 * * * /home/pi/rassl/rassl_display.sh off >/dev/null 2>&1

    # Turn on display between 5pm and 8pm
    0 17 * * * /home/pi/rassl/rassl_display.sh on >/dev/null 2>&1
    0 20 * * * /home/pi/rassl/rassl_display.sh off >/dev/null 2>&1
