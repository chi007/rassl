#!/bin/bash

RASSL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "RASSL_DIR=$RASSL_DIR"

if [ "$1" != "" ]; then
  set RASSL_DIR=$1
fi

SLIDES_DIR="$RASSL_DIR/slides"
mkdir -p $SLIDES_DIR

find $HOME/in_slides/*.jpg -print | while read filename; do
UUID=$(cat /proc/sys/kernel/random/uuid)
echo "renaming $filename to $SLIDES_DIR/$UUID.jpg..."
mv -v $filename $SLIDES_DIR/$UUID.jpg
done
:
# make all filenames lowercase
#rename 'y/A-Z/a-z/' $SLIDES_DIR/*

# add files lists
ls -td1 $SLIDES_DIR/*.jpg | head -n 10 > $SLIDES_DIR/fl_last_10.txt
ls -td1 $SLIDES_DIR/*.jpg | head -n 20 > $SLIDES_DIR/fl_last_20.txt
ls -td1 $SLIDES_DIR/*.jpg | head -n 40 > $SLIDES_DIR/fl_last_40.txt
ls -td1 $SLIDES_DIR/*.jpg              > $SLIDES_DIR/fl_all.txt


find $SLIDES_DIR/*.jpg -print | while read filename; do

echo "autorotating $filename..."
# remember last modified date
rlmd=$(date -R -r "$filename")
# rotate files
jhead -autorot "$filename"
# set last modified date
touch -d "$rlmd" "$filename"

done 

# restart slideshow
pkill fbi
