#!/bin/bash

sudo apt-get update

# basic packages
sudo apt-get install -q fbi imagemagick jhead rsync raname pkill

# collecting images from email account
sudo apt-get install -q fetchmail procmail uudeview
